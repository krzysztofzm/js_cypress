const amount = 2;
const searchedItem = 'kwiat'

context('Actions', () => {
  beforeEach(() => {
    cy.visit('https://allegro.pl/')
    cy.get('[data-role=accept-consent]').click()
  })
  it('Search for items and add them to cart', () => {
    //searches for item in searchbar
    cy.searchFor(searchedItem)
    //iterates through result list and adds given amount of items
    cy.addToCart(amount)
    //verifies if the cart contains proper amount of items
    cy.verifyCart(amount)
  })
})
