Cypress.Commands.add('searchFor', (item) => {
  cy.get('[data-role=search-input]')
    .type(item).should('have.value', item)
  cy.get('[data-role=search-button]').click()
})

Cypress.Commands.add('addToCart', (amountOfItems) => {
  for (var i = 0; i < amountOfItems; i++){
    cy.get('span.mpof_5r._9c44d_1CbiU').eq(i)
      .click()
    cy.get('[alt=zamknij]', { timeout: 10000 }).should('be.visible')
      .click()
  }
})

Cypress.Commands.add('verifyCart', (expectedAmount) => {
  cy.get('[data-role=cart-quantity]').contains(expectedAmount)
})
