class Numbers {
  constructor(number) {
    this.number = number;
  }

  //this method is searching for prime numbers in range from 0 till this.number
  prime(){
    var allNumbers = [];
    var result = [];
    for (var i=1; i < this.number + 1; i++){
      allNumbers.push(i);
    }

    allNumbers.forEach((i) => {
      if(i === 2) {
        result.push(i);
      } else if (i > 1) {
        let count = 0;
        for(var k = 2; k < i; k++){
          if(i % k !== 0){
            count++;
          }
        }
        if(count === i - 2){
            result.push(i);
          }
        }
    });
    //console.log(result);
    return result;
  }

  //this method logs a given number (this.number) of prime numbers
  nPrimeNumbers(){
    var result = [];
    let i = 2;
    while (result.length < this.number) {
      if(i === 2) {
        result.push(i);
    }
    else {
      let count = 0;
      for(var k = 2; k < i; k++){
        if(i % k !== 0){
          count++;
        }
      }
      if(count === i - 2){
          result.push(i);
        }
      }
      i++
    };
    return result;
  }
}

var n = new Numbers(100);
//n.prime();
console.log(n.prime());
